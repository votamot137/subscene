import logging
import os

from flask import Flask, Blueprint, request, jsonify, redirect

from helpers.subscene import Subscene
from helpers.exceptions import SubsceneException


instance = __file__.split("/")[-1].split(".")[0]
logger = logging.getLogger()
logger.setLevel(logging.DEBUG if os.environ.get("DEBUG") == 1 else logging.INFO)
app = Flask(__name__)
app.config["subscene_client"] = Subscene()
api = Blueprint(f"api_{instance}", __name__)


@api.route("/", methods=["POST"])
def _search():
    data = request.get_json()
    logger.info("input to function: %s", data)
    query = data.get("query", "")
    if not query:
        return jsonify({"status": False, "message": "No input"}), 403

    subscene_client = app.config["subscene_client"]
    try:
        search_results = subscene_client.search(query)
        return jsonify({"status": True, "data": search_results}), 200
    except SubsceneException as e:
        logger.warning(e)
        return jsonify({"status": False, "message": str(e)}), e.status_code
    except Exception as e:
        logger.error(e, exc_info=True)
        return jsonify({"status": False, "message": "Internal server error"}), 500


@api.route("/", methods=["PUT"])
def _movie():
    data = request.get_json()
    logger.info("input to function: %s", data)
    movie_id = data.get("movie_id", "")
    if not movie_id:
        return jsonify({"status": False, "message": "No input"}), 403

    subscene_client = app.config["subscene_client"]
    try:
        movie_details = subscene_client.get_movie(movie_id)
        return jsonify({"status": True, "data": movie_details}), 200
    except SubsceneException as e:
        logger.warning(e)
        return jsonify({"status": False, "message": str(e)}), e.status_code
    except Exception as e:
        logger.error(e, exc_info=True)
        return jsonify({"status": False, "message": "Internal server error"}), 500


@api.route("/", methods=["GET"])
def _download():
    movie_id = request.args.get("movie_id")
    subtitle_id = request.args.get("subtitle_id")
    lang = request.args.get("lang")
    if not movie_id or not subtitle_id or not lang:
        return jsonify({"nothing": "here"}), 404

    subscene_client = app.config["subscene_client"]
    try:
        redirect_url = subscene_client.get_subtitle_download_link(
            movie_id, subtitle_id, lang
        )
        return redirect(redirect_url)
    except SubsceneException as e:
        logger.warning(e)
        return jsonify({"status": False, "message": str(e)}), e.status_code
    except Exception as e:
        logger.error(e, exc_info=True)
        return jsonify({"status": False, "message": "Internal server error"}), 500


app.register_blueprint(api, url_prefix=f"/api/{instance}")
