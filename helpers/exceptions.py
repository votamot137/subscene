class SubsceneException(Exception):
    def __init__(self, message, *args, status_code=500):
        super().__init__(message, *args)
        self.status_code = status_code
