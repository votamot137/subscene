from urllib.parse import urljoin
from collections import defaultdict

from requests import Session
from bs4 import BeautifulSoup

from .exceptions import SubsceneException


class Subscene:
    def __init__(self):
        self._base_url = "https://subscene.com"
        self._headers = {
            "user-agent": "Mozilla/5.0 (X11; Linux armv7l) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu "
            "Chromium/70.0.3538.67 Chrome/70.0.3538.67 Safari/537.36",
            "origin": self._base_url,
        }
        self._session = Session()

    def _request(self, method, url, **kwargs):
        return self._session.request(
            method=method, url=url, headers=self._headers, **kwargs
        )

    def search(self, query):
        search_url = urljoin(self._base_url, "/subtitles/searchbytitle")
        resp = self._request(method="POST", url=search_url, data={"query": query})
        if resp.status_code >= 400:
            raise SubsceneException("Server unavailable right now")

        html = resp.text
        soup = BeautifulSoup(html, "html.parser")
        search_result = soup.find_all("div", class_="search-result")
        if not search_result:
            raise SubsceneException(
                f"Sorry I couldn't find any result for your query {query}!",
                status_code=404,
            )

        search_result = search_result[0]
        all_a = search_result.find_all("a")
        results = []
        paths = set()
        for a in all_a:
            path = a.get("href").split("/")[-1]
            if path in paths:
                continue
            paths.add(path)
            title = a.string.strip()
            results.append((path, title))
        return results

    def get_movie(self, movie_id):
        movie_page_url = urljoin(self._base_url, f"/subtitles/{movie_id}")
        resp = self._request(method="GET", url=movie_page_url)
        if resp.status_code >= 400:
            raise SubsceneException("Server unavailable right now")

        html = resp.text
        soup = BeautifulSoup(html, "html.parser")
        table = soup.find_all("table")
        if not table:
            raise SubsceneException(
                "I couldn't find any result for the query!", status_code=404
            )

        table = table[0]
        tds = table.find_all("td", class_="a1")
        results = defaultdict(list)
        seen_ids = defaultdict(set)
        for td in tds:
            a = td.a
            href = a.get("href")
            span = a.find_all("span")[-1]
            lang, code = href.split("/")[-2:]
            title = span.string.strip()
            if code in seen_ids[lang]:
                continue
            seen_ids[lang].add(code)
            results[lang].append({"id": code, "title": title})
        return results

    def get_subtitle_download_link(self, movie_id, subtitle_id, lang):
        subtitle_page_url = urljoin(
            self._base_url, f"/subtitles/{movie_id}/{lang}/{subtitle_id}"
        )
        resp = self._request(method="GET", url=subtitle_page_url)
        if resp.status_code >= 400:
            raise SubsceneException("Server unavailable right now")

        html = resp.text
        soup = BeautifulSoup(html, "html.parser")
        btn = soup.find_all("a", id="downloadButton")
        if not btn:
            raise SubsceneException("Subtitle not available", status_code=404)
        btn = btn[0]
        return urljoin(self._base_url, btn.get("href"))
